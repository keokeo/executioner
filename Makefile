PROJECT_NAME=executioner
GO_ENV=GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64
GO_FILES=$(shell go list ./... | grep -v /vendor/)

VERSION=$(shell cat VERSION)
COMMIT_HASH=$(shell git rev-parse --short HEAD)
APP_VERSION=$(VERSION)-$(COMMIT_HASH)

all: fmt vet test install

fmt:
	$(GO_ENV) go fmt $(GO_FILES)

vet:
	$(GO_ENV) go vet $(GO_FILES)

check:
	staticcheck $(GO_FILES)

test:
	$(GO_ENV) go test -cover -v $(GO_FILES)

integration-test:
	$(GO_ENV) go test -tags=integration -cover -v $(GO_FILES)

install:
	$(GO_ENV) go install

vendor:
	$(GO_ENV) go mod vendor

build:
	$(GO_ENV) go build -ldflags="-X main.version=$(APP_VERSION)" -v -o $(PROJECT_NAME).bin .

run:
	$(GO_ENV) go run -ldflags="-X main.version=$(APP_VERSION)" main.go -stage=dev -config=configs
