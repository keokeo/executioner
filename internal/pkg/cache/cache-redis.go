package cache

import (
	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/pkg/health"

	"time"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type Cache struct {
	conf        config.Redis
	redisClient *redis.Client
}

func New(conf config.Redis) (*Cache, error) {
	redisClient := redis.NewClient(&redis.Options{
		Addr:     conf.Address,
		Password: conf.Password,
	})
	_, err := redisClient.Ping().Result()
	if err != nil {
		return nil, errors.Wrapf(err, "failed to ping Redis")
	}

	return &Cache{
		conf:        conf,
		redisClient: redisClient,
	}, nil
}

func (c *Cache) Get(key string) (val string, err error) {
	return c.redisClient.Get(key).Result()
}

func (c *Cache) Set(key string, value interface{}) (err error) {
	_, err = c.redisClient.Set(key, value, 0).Result()
	return err
}

func (c *Cache) SetWithDeadline(key string, value interface{}, expiration time.Duration) (err error) {
	_, err = c.redisClient.Set(key, value, expiration).Result()
	return err
}

func (c *Cache) Delete(key string) error {
	_, err := c.redisClient.Del(key).Result()
	return err
}

func (c *Cache) Close() error {
	if c.redisClient != nil {
		return c.redisClient.Close()
	}
	return nil
}

func (c *Cache) Ping() []*health.Endpoint {
	e := health.Endpoint{
		Name:      "redis",
		Addresses: []string{c.conf.Address},
		Status:    health.OK,
	}
	if err := c.redisClient.Ping().Err(); err != nil {
		e.Status = health.UNHEALTHY
		e.Error = err.Error()
	}
	return []*health.Endpoint{&e}
}
