package dev_challenge

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

const (
	devChallenges = "dev_challenges" // Collection name
)

type MongoDBRepo struct {
	session *mgo.Session
}

func NewMongoDBRepo(session *mgo.Session) *MongoDBRepo {
	return &MongoDBRepo{
		session: session,
	}
}

func (r *MongoDBRepo) CreateChallenge(dc *model.DevChallenge) (created *model.DevChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	if err := sess.DB("").C(devChallenges).Insert(dc); err != nil {
		return nil, err
	}
	return r.GetChallenge(dc.ID)
}

func (r *MongoDBRepo) GetChallenge(id string) (dc *model.DevChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	dc = &model.DevChallenge{}
	err = sess.DB("").C(devChallenges).Find(bson.M{"id": id}).One(dc)
	if err != nil {
		return nil, err
	}
	return dc, nil
}

func (r *MongoDBRepo) ListChallenges(query interface{}, offset, limit int) (dcs []*model.DevChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	if limit <= 0 {
		limit = config.DefaultPageSize
	}

	dcs = make([]*model.DevChallenge, 0)
	err = sess.DB("").C(devChallenges).Find(query).Sort("-createdAt").Skip(offset).Limit(limit).All(&dcs)
	if err != nil {
		return nil, err
	}
	return dcs, nil
}

func (r *MongoDBRepo) UpdateChallenge(id string, updater interface{}) (dc *model.DevChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	err = sess.DB("").C(devChallenges).Update(bson.M{"id": id}, bson.M{"$set": updater})
	if err != nil {
		return nil, err
	}
	return r.GetChallenge(id)
}

func (r *MongoDBRepo) DeleteChallenge(id string) (err error) {
	sess := r.session.Clone()
	defer sess.Close()

	err = sess.DB("").C(devChallenges).Remove(bson.M{"id": id})
	if err != nil {
		return err
	}
	return nil
}

func (r *MongoDBRepo) GetChallengeNames(cids []string) (cnames []challengeName, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	cnames = make([]challengeName, 0)
	err = sess.DB("").C(devChallenges).Find(bson.M{"id": bson.M{"$in": cids}}).
		Select(bson.M{"id": 1, "title": 1}).All(&cnames)
	if err != nil {
		return nil, err
	}
	return cnames, nil
}
