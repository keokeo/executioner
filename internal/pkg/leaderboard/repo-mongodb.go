package leaderboard

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

const (
	users                = "users"                  // Collection name
	userSolvedChallenges = "user_solved_challenges" // Collection name
)

type MongoDBRepo struct {
	session *mgo.Session
}

func NewMongoDBRepo(session *mgo.Session) *MongoDBRepo {
	return &MongoDBRepo{
		session: session,
	}
}

func (r *MongoDBRepo) GetGlobalLeaderboard() (lb []*model.User, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	lb = make([]*model.User, 0)
	err = sess.DB("").C(users).
		Find(bson.M{"cf.xp": bson.M{"$gt": 0}}).
		Sort("-cf.xp", "-cf.solved", "-cf.updatedAt").
		All(&lb)
	if err != nil {
		return nil, err
	}
	return lb, nil
}

func (r *MongoDBRepo) GetChallengeLeaderboard(cid string) (lb []*model.UserSolvedChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	lb = make([]*model.UserSolvedChallenge, 0)
	err = sess.DB("").C(userSolvedChallenges).
		Find(bson.M{"$and": []bson.M{{"cid": cid}, {"isSolved": true}}}).
		Select(bson.M{"versions": bson.M{"$slice": 0}}).
		Sort("fastestRun", "solvedAt").
		All(&lb)
	if err != nil {
		return nil, err
	}
	return lb, nil
}

func (r *MongoDBRepo) GetChallengeLeaderboardByTimeRange(cid string, start, end int64) (lb []*model.UserSolvedChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	lb = make([]*model.UserSolvedChallenge, 0)
	query := bson.M{
		"$and": []bson.M{
			{"cid": cid},
			{"isSolved": true},
			{"$and": []bson.M{
				{"solvedAt": bson.M{"$gte": start}},
				{"solvedAt": bson.M{"$lt": end}},
			}},
		},
	}

	err = sess.DB("").C(userSolvedChallenges).
		Find(query).
		Select(bson.M{"versions": bson.M{"$slice": 0}}).
		Sort("fastestRun", "solvedAt").
		All(&lb)
	if err != nil {
		return nil, err
	}
	return lb, nil
}
