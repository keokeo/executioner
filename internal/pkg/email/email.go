package email

import (
	"net/mail"

	"github.com/go-gomail/gomail"
	"github.com/matcornic/hermes/v2"
	"github.com/pkg/errors"

	"gitlab.com/fighters-solution/executioner/internal/config"
)

type Sender struct {
	conf     config.Email
	mailTmpl *hermes.Hermes
}

func NewSender(conf config.Email) (*Sender, error) {
	hm := hermes.Hermes{
		Product: hermes.Product{
			Name:      "Code Fighter",
			Link:      "https://codefighter.app/",
			Copyright: "Copyright © 2019 Code Fighter. All rights reserved.",
			Logo:      "https://lh3.googleusercontent.com/f2qdtx67zjMla0LLjIs4KAorXvL7xc189oWmiXNxmdB2FVHSVdebNO0ox2NaAlDbonzMMv5X3EZhptAXqt9hmKe8_qAvqMx0Xg-1Bkr5-yq4JphyhSmTPYeV9YbTs3Q5pnklQLcf=w512",
		},
	}

	return &Sender{
		conf:     conf,
		mailTmpl: &hm,
	}, nil
}

func (s *Sender) SendConfirmEmail(uid, toEmail, confirmURL string) error {
	mailBody := hermes.Email{
		Body: hermes.Body{
			Name: uid,
			Intros: []string{
				"Welcome to Code Fighter! We're very excited to have you on board.",
			},
			Actions: []hermes.Action{
				{
					Instructions: "To get started with Code Fighter, please click here:",
					Button: hermes.Button{
						Color: "#22BC66",
						Text:  "Confirm your account",
						Link:  confirmURL,
					},
				},
			},
			Outros: []string{
				"Note: The confirmation link will be expired after 15 minutes.",
				"Need help, or have questions? Just reply to this email, we'd love to help.",
			},
		},
	}

	txtBody, err := s.mailTmpl.GeneratePlainText(mailBody)
	if err != nil {
		return errors.Wrapf(err, "failed to generate plaintext email body")
	}
	htmlBody, err := s.mailTmpl.GenerateHTML(mailBody)
	if err != nil {
		return errors.Wrapf(err, "failed to generate HTML email body: %v", err)
	}

	from := mail.Address{
		Name:    s.conf.SenderName,
		Address: s.conf.SenderEmail,
	}
	m := gomail.NewMessage()
	m.SetHeader("From", from.String())
	m.SetHeader("To", toEmail)
	m.SetHeader("Subject", "Welcome to CodeFighter")

	m.SetBody("text/plain", txtBody)
	m.AddAlternative("text/html", htmlBody)

	dialer := gomail.NewDialer(s.conf.ServerHost, s.conf.ServerPort, s.conf.Username, s.conf.Password)
	return dialer.DialAndSend(m)
}
