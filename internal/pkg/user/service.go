package user

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/go-chi/chi"
	"github.com/go-playground/validator"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/internal/session"

	"gitlab.com/fighters-solution/executioner/pkg/util"
	"gitlab.com/fighters-solution/executioner/pkg/util/request"
	"gitlab.com/fighters-solution/executioner/pkg/util/response"
)

const (
	authTypeLocal = "local"
	authTypeOAuth = "oauth"
)

var (
	log = logrus.WithField("pkg", "user")

	userNoUpdateFields = []string{"id", "uid", "password", "email", "role", "cf", "createAt", "updatedAt", "settings"}

	allowedCharsRegex = regexp.MustCompile(`^[A-Za-z0-9]+$`)
)

type (
	Service struct {
		isEnableEmailConfirm bool
		hostURL              string
		mux                  *sync.RWMutex

		userRepo    Repo
		avatarRepo  AvatarRepo
		cache       Cache
		emailSender EmailSender

		defaultAvatar []byte
	}

	Repo interface {
		CreateUser(u *model.User) (created *model.User, err error)
		GetUser(id string) (user *model.User, err error)
		GetUserByQuery(query interface{}) (user *model.User, err error)
		UpdateUser(id string, updater interface{}) (updated *model.User, err error)
	}

	Cache interface {
		Get(key string) (val string, err error)
		Delete(key string) error
		SetWithDeadline(key string, val interface{}, expiration time.Duration) (err error)
	}

	EmailSender interface {
		SendConfirmEmail(uid, email, confirmURL string) error
	}

	AvatarRepo interface {
		GetUserAvatar(id string) (avatar *model.UserAvatar, err error)
		UpdateUserAvatar(id string, updater interface{}) (avatar *model.UserAvatar, err error)
		DeleteUserAvatar(id string) (err error)
	}
)

func NewService(userRepo Repo, avatarRepo AvatarRepo, cache Cache, emailSender EmailSender, hostURL, defaultAvatarPath string, isEnableEmailConfirm bool) *Service {
	s := &Service{
		isEnableEmailConfirm: isEnableEmailConfirm,
		mux:                  &sync.RWMutex{},
		userRepo:             userRepo,
		avatarRepo:           avatarRepo,
		cache:                cache,
		emailSender:          emailSender,
		hostURL:              hostURL,
	}
	if err := s.loadDefaultAvatar(defaultAvatarPath); err != nil {
		log.Warnf("failed to load default avatar: %v", err)
	}
	return s
}

func (s *Service) loadDefaultAvatar(defaultAvatarPath string) error {
	if defaultAvatarPath == "" {
		return errors.New("invalid default avatar path")
	}
	f, err := os.OpenFile(defaultAvatarPath, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return errors.Wrap(err, "failed to load default avatar to memory cache")
	}
	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		return errors.Wrap(err, "failed to read default avatar image")
	}
	s.defaultAvatar = b
	return nil
}

func (s *Service) ConfigChanged(conf config.Config) error {
	s.mux.Lock()
	defer s.mux.Unlock()

	if err := s.loadDefaultAvatar(conf.Misc.DefaultAvatar); err != nil {
		log.Warnf("failed to load default avatar: %v", err)
		return nil
	}
	log.Debugf("user default avatar config updated")
	return nil
}

func (s *Service) Register(w http.ResponseWriter, r *http.Request) {
	regType := request.Query(r, "type")

	switch strings.ToLower(regType) {
	case authTypeLocal:
		s.registerLocalUser(w, r)
	case authTypeOAuth:
		// TODO
		response.ErrorCode(w, http.StatusNotImplemented)
	default:
		response.ErrorCode(w, http.StatusBadRequest)
	}
}

func (s *Service) registerLocalUser(w http.ResponseWriter, r *http.Request) {
	type regForm struct {
		Username   string `json:"account" validate:"required,min=4,max=32"`
		Email      string `json:"email" validate:"required,email"`
		Password   string `json:"password" validate:"required,min=6,max=32"`
		RePassword string `json:"rePassword" validate:"required,min=6,max=32"`
		RegType    string `json:"regType"`
	}
	reg := regForm{}
	if err := json.NewDecoder(r.Body).Decode(&reg); err != nil {
		log.Errorf("failed to decode register API body to register form: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	if reg.RegType != string(authTypeLocal) {
		log.Errorf("invalid authentication type: %v", reg.RegType)
		response.Error(w, fmt.Errorf("invalid authentication type: %s", reg.RegType), http.StatusBadRequest)
		return
	}
	// Validate password
	if reg.Password != reg.RePassword {
		response.Error(w, fmt.Errorf("password not matched"), http.StatusBadRequest)
		return
	}
	if !allowedCharsRegex.MatchString(reg.Username) {
		response.Error(w, fmt.Errorf("username contains invalid character(s)"), http.StatusBadRequest)
		return
	}
	if err := validator.New().Struct(reg); err != nil {
		log.Errorf("failed to validate user register form: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	pwdEncrypted, err := bcrypt.GenerateFromPassword([]byte(reg.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Errorf("failed to hash password using Bcrypt: %v", err)
		response.Error(w, fmt.Errorf("invalid password"), http.StatusBadRequest)
		return
	}

	// Check user already registered
	query := bson.M{"$or": []bson.M{{"uid": reg.Username}, {"email": reg.Email}}}
	tmp, err := s.userRepo.GetUserByQuery(query)
	if err != nil && err != mgo.ErrNotFound {
		log.Errorf("failed to check user existence: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	if err == nil {
		log.Errorf("user creation duplicated: %s - %s", reg.Username, reg.Email)
		errMsg := ""
		if reg.Username == tmp.UID {
			errMsg = "Username duplicated"
		} else if reg.Email == tmp.Email {
			errMsg = "Email duplicated"
		}
		response.Error(w, fmt.Errorf(errMsg), http.StatusBadRequest)
		return
	}

	// Register user
	now := time.Now().Unix()
	isEmailConfirmed := false
	if !s.isEnableEmailConfirm {
		isEmailConfirmed = true
	}
	u := model.User{
		ID:       util.UUID(),
		UID:      reg.Username,
		Name:     reg.Username,
		Email:    reg.Email,
		Password: string(pwdEncrypted),
		Role:     model.NormalUser,
		Confirm: model.Confirm{
			Email:            reg.Email,
			IsEmailConfirmed: isEmailConfirmed,
		},
		CreatedAt: now,
		UpdatedAt: now,
	}
	created, err := s.userRepo.CreateUser(&u)
	if err != nil {
		log.Errorf("failed to save user: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	if s.isEnableEmailConfirm {
		// Store token to cache
		confirmToken := util.UUID()
		err = s.cache.SetWithDeadline(confirmToken, fmt.Sprintf(`{"type":"confirm","uid":"%s","email":"%s"}`, reg.Username, reg.Email), config.DefaultConfirmTokenExpiration)
		if err != nil {
			log.Errorf("failed to save confirm token to cache: %v", err)
			response.ErrorCode(w, http.StatusInternalServerError)
			return
		}
		// Send confirm email
		confirmURL := fmt.Sprintf("%s/confirm?token=%s", s.hostURL, confirmToken)
		err = s.emailSender.SendConfirmEmail(reg.Username, reg.Email, confirmURL)
		if err != nil {
			log.Errorf("failed to send confirm email: %v", err)
			response.ErrorCode(w, http.StatusInternalServerError)
			return
		}
	}

	response.JSON(w, http.StatusOK, created)
}

func (s *Service) ConfirmEmail(w http.ResponseWriter, r *http.Request) {
	token := request.Query(r, "token")
	if token == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	data, err := s.cache.Get(token)
	if err != nil {
		log.Errorf("failed to find confirmation token: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	type confirmInfo struct {
		Type  string `json:"type"`
		UID   string `json:"uid"`
		Email string `json:"email"`
	}
	ci := confirmInfo{}
	if err := json.Unmarshal([]byte(data), &ci); err != nil {
		log.Errorf("failed to decode confirm data: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	if ci.Type != "confirm" {
		log.Errorf("token found but data not in confirm type: %s - %v", token, data)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	updater := bson.M{"email": ci.Email, "updatedAt": time.Now().Unix(), "confirm.isEmailConfirmed": true, "confirm.email": ""}
	if _, err := s.userRepo.UpdateUser(ci.UID, updater); err != nil {
		log.Errorf("failed to update user email confirmation: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	if err := s.cache.Delete(token); err != nil {
		// Log only, still response back to user as success
		log.Errorf("failed to remove token from cache: %v", err)
	}

	response.JSON(w, http.StatusOK, "")
}

func (s *Service) SendConfirmEmail(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "uid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	u, err := s.userRepo.GetUser(id)
	if err != nil {
		log.Errorf("failed to get user: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	updater := bson.M{"confirm.email": u.Email, "confirm.isEmailConfirmed": false, "updatedAt": time.Now().Unix()}
	u, err = s.userRepo.UpdateUser(id, updater)
	if err != nil {
		log.Errorf("failed to update user: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	if s.isEnableEmailConfirm {
		// Store token to cache
		confirmToken := util.UUID()
		err = s.cache.SetWithDeadline(confirmToken, fmt.Sprintf(`{"type":"confirm","uid":"%s","email":"%s"}`, id, u.Email), config.DefaultConfirmTokenExpiration)
		if err != nil {
			log.Errorf("failed to save confirm token to cache: %v", err)
			response.ErrorCode(w, http.StatusInternalServerError)
			return
		}
		// Send confirm email
		confirmURL := fmt.Sprintf("%s/confirm?token=%s", s.hostURL, confirmToken)
		err = s.emailSender.SendConfirmEmail(id, u.Email, confirmURL)
		if err != nil {
			log.Errorf("failed to send confirm email: %v", err)
			response.ErrorCode(w, http.StatusInternalServerError)
			return
		}
	}

	response.JSON(w, http.StatusOK, "")
}

func (s *Service) GetUserProfile(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "uid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	u, err := s.userRepo.GetUser(id)
	if err != nil {
		log.Errorf("failed to get user: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	u.OmitPassword()

	{
		// Hide email if the requester are not the owner of resource or admin.
		us, err := session.GetUser(r)
		if err != nil {
			log.Tracef("getSession error: %v", err)
			goto OMIT_RETURN
		}

		if us.UID == u.UID || model.UserRole(us.Role) == model.Admin {
			response.JSON(w, http.StatusOK, u)
			return
		}

		goto OMIT_RETURN
	}

OMIT_RETURN:
	u.OmitEmail()
	response.JSON(w, http.StatusOK, u)
}

// For user update their basic info only.
// Admin updates user info or user update settings/password should use other APIs.
func (s *Service) UpdateUserProfile(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "uid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	updater := make(map[string]interface{})
	err := json.NewDecoder(r.Body).Decode(&updater)
	if err != nil {
		log.Errorf("failed to decode update API to user profile: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	for _, field := range userNoUpdateFields {
		delete(updater, field)
	}
	updater["updatedAt"] = time.Now().Unix()
	updated, err := s.userRepo.UpdateUser(id, updater)
	if err != nil {
		log.Errorf("failed to update user profile: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	updated.OmitPassword()
	response.JSON(w, http.StatusOK, updated)
}

func (s *Service) UpdateUserSettings(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "uid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	settings := model.Settings{}
	err := json.NewDecoder(r.Body).Decode(&settings)
	if err != nil {
		log.Errorf("failed to decode update API body to user settings: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	updated, err := s.userRepo.UpdateUser(id, bson.M{"settings": settings})
	if err != nil {
		log.Errorf("failed to update user settings: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	updated.OmitPassword()
	response.JSON(w, http.StatusOK, updated)
}

func (s *Service) GetUserAvatar(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "uid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	avatar, err := s.avatarRepo.GetUserAvatar(id)
	if err != nil {
		log.Tracef("failed to get user avatar: %v", err)
		w.Header().Set("Content-Type", "image/png") // TODO: detect image type
		w.Header().Set("Content-Length", strconv.Itoa(len(s.defaultAvatar)))
		_, _ = w.Write(s.defaultAvatar)
		return
	}

	commaIdx := strings.Index(avatar.Raw, ",")
	if commaIdx < 0 {
		log.Warnf("invalid user avatar: %v", err)
		w.Header().Set("Content-Type", "image/png") // TODO: detect image type
		w.Header().Set("Content-Length", strconv.Itoa(len(s.defaultAvatar)))
		_, _ = w.Write(s.defaultAvatar)
		return
	}
	decodedImgReader := base64.NewDecoder(base64.StdEncoding, strings.NewReader(avatar.Raw[commaIdx+1:]))
	w.Header().Set("Content-Type", avatar.Format)
	if _, err = io.Copy(w, decodedImgReader); err != nil {
		log.Errorf("failed to response user avatar: %v", err)
	}
}

// TODO: Validate image data
func (s *Service) SetUserAvatar(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "uid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	updater := make(map[string]interface{})
	if err := json.NewDecoder(r.Body).Decode(&updater); err != nil {
		log.Errorf("failed to decode set user updated body to updated: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if _, err := s.avatarRepo.GetUserAvatar(id); err == nil { // Avatar existed => Update
		delete(updater, "id")
	} else if err == mgo.ErrNotFound { // Avatar not existed => Insert new one
		updater["id"] = id
	} else {
		log.Errorf("failed to get user avatar: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	updated, err := s.avatarRepo.UpdateUserAvatar(id, updater)
	if err != nil {
		log.Errorf("failed to set user avatar: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, updated)
}

func (s *Service) DeleteUserAvatar(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "uid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if err := s.avatarRepo.DeleteUserAvatar(id); err != nil {
		log.Errorf("failed to delete user avatar: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, "")
}

func (s *Service) Login(w http.ResponseWriter, r *http.Request) {
	type loginForm struct {
		Account  string `json:"account"`
		Password string `json:"password"`
	}
	login := loginForm{}
	if err := json.NewDecoder(r.Body).Decode(&login); err != nil {
		log.Errorf("failed to decode create API body to login form: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	query := bson.M{"$or": []bson.M{{"uid": login.Account}, {"email": login.Account}}}
	user, err := s.userRepo.GetUserByQuery(query)
	if err != nil {
		log.Errorf("login failed, account not found: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(login.Password)); err != nil {
		log.Errorf("login failed, password not matched: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if !user.Confirm.IsEmailConfirmed && user.Confirm.Email != "" {
		session.SetCookieOnly(w, model.CookieConfirm, "true")
	}
	sess, err := session.New(r, model.CookieSession)
	if err != nil {
		log.Errorf("failed to create user session: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	sess.Values["user"] = util.ToJSONString(model.UserSession{
		UID:  user.UID,
		Role: int(user.Role),
		Name: user.Name,
	})
	if err := sess.Save(r, w); err != nil {
		log.Errorf("failed to save user session: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	user.OmitPassword()
	user.OmitEmail()
	response.JSON(w, http.StatusOK, user)
}

func (s *Service) Logout(w http.ResponseWriter, r *http.Request) {
	sess, err := session.Get(r, model.CookieSession)
	if err != nil {
		log.Errorf("failed to get session from request: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	sess.Values = nil
	sess.Options.MaxAge = 0
	if err := sess.Save(r, w); err != nil {
		log.Errorf("failed to delete session: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, "")
}
