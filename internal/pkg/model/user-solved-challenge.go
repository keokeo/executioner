package model

type (
	UserSolvedChallenge struct {
		ID         string    `json:"id" bson:"id"`
		UID        string    `json:"uid" bson:"uid"`
		CID        string    `json:"cid" bson:"cid"`
		Versions   []Version `json:"versions" bson:"versions"`
		FastestRun float64   `json:"fastestRun" bson:"fastestRun"`
		SolvedAt   int64     `json:"solvedAt" bson:"solvedAt"`
		Language   string    `json:"language" bson:"language"`
		IsSolved   bool      `json:"isSolved" bson:"isSolved"`
		Point      int       `json:"point" bson:"point"`
		CreatedAt  int64     `json:"createdAt" bson:"createdAt"`
		UpdatedAt  int64     `json:"updatedAt" bson:"updatedAt"`
	}

	Version struct {
		// TODO: Should use ID instead of SubmittedAt to get the user solved change version.
		//  As SubmittedAt is a Unix timestamp, it exposes an attack vector to the API.
		//  E.g.: Brute-force to get user challenge's solution (we have rate-limit so it's ok but
		//  still better to improve this).
		//  More on `user-solved-challenge > repo-mongodb.go > GetSolution#73`
		// ID          string  `json:"id" bson:"id"`

		SubmittedAt int64   `json:"submittedAt" bson:"submittedAt"`
		Language    string  `json:"language" bson:"language"`
		IsSolved    bool    `json:"isSolved" bson:"isSolved"`
		ExecTime    float64 `json:"execTime" bson:"execTime"`
		Code        string  `json:"code" bson:"code"`
	}
)
