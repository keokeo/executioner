package model

type (
	DevChallenge struct {
		ID          string   `json:"id" bson:"id"`
		Title       string   `json:"title" bson:"title"`
		Description string   `json:"description" bson:"description"`
		ShortDesc   string   `json:"shortDesc" bson:"shortDesc"`
		Point       int      `json:"point" bson:"point"`
		Disabled    bool     `json:"disabled" bson:"disabled"`
		SolvedBy    int      `json:"solvedBy" bson:"solvedBy"`
		PostedBy    string   `json:"postedBy" bson:"postedBy"`
		CreatedAt   int64    `json:"createdAt" bson:"createdAt"`
		UpdatedAt   int64    `json:"updatedAt" bson:"updatedAt"`
		Codes       []Code   `json:"codes" bson:"codes"`
		Leagues     []string `json:"leagues" bson:"leagues"`
		Template    string   `json:"template" bson:"template"`
	}

	Code struct {
		Language string `json:"language" bson:"language"`
		Template string `json:"template" bson:"template"`
		Fixture  string `json:"fixture" bson:"fixture"`
		TestCase string `json:"testCase" bson:"testCase"`
		Timeout  int64  `json:"timeout" bson:"timeout"`
	}
)

func (dc *DevChallenge) OmitSensitiveData() {
	dc.Template = ""
	for i := 0; i < len(dc.Codes); i++ {
		dc.Codes[i].Fixture = ""
		dc.Codes[i].TestCase = ""
	}
}
