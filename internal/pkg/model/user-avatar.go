package model

type UserAvatar struct {
	ID     string `json:"id" bson:"id"`
	Format string `json:"format" bson:"format"`
	Raw    string `json:"raw" bson:"raw"`
}
