package league

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

const (
	leagues = "leagues" // Collection name
)

type (
	MongoDBRepo struct {
		session *mgo.Session
	}

	leagueName struct {
		ID    string `json:"id" bson:"id"`
		Title string `json:"title" bson:"title"`
	}
)

func NewMongoDBRepo(session *mgo.Session) *MongoDBRepo {
	return &MongoDBRepo{
		session: session,
	}
}

func (r *MongoDBRepo) CreateLeague(league *model.League) (created *model.League, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	if err := sess.DB("").C(leagues).Insert(league); err != nil {
		return nil, err
	}
	return r.GetLeague(league.ID)
}

func (r *MongoDBRepo) GetLeague(id string) (league *model.League, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	league = &model.League{}
	err = sess.DB("").C(leagues).Find(bson.M{"id": id}).One(league)
	if err != nil {
		return nil, err
	}
	return league, nil
}

func (r *MongoDBRepo) ListLeagues(query interface{}, offset, limit int) (ls []*model.League, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	ls = make([]*model.League, 0)
	err = sess.DB("").C(leagues).Find(query).Skip(offset).Limit(limit).All(&ls)
	if err != nil {
		return nil, err
	}
	return ls, nil
}

func (r *MongoDBRepo) UpdateLeague(id string, updater interface{}) (league *model.League, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	err = sess.DB("").C(leagues).Update(bson.M{"id": id}, bson.M{"$set": updater})
	if err != nil {
		return nil, err
	}
	return r.GetLeague(id)
}

func (r *MongoDBRepo) DeleteLeague(id string) (err error) {
	sess := r.session.Clone()
	defer sess.Close()

	err = sess.DB("").C(leagues).Remove(bson.M{"id": id})
	if err != nil {
		return err
	}
	return nil
}

func (r *MongoDBRepo) GetLeagueNames(lids []string) (lnames []leagueName, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	lnames = make([]leagueName, 0)
	err = sess.DB("").C(leagues).Find(bson.M{"id": bson.M{"$in": lids}}).
		Select(bson.M{"id": 1, "title": 1}).All(&lnames)
	if err != nil {
		return nil, err
	}
	return lnames, nil
}
