package schedule

import (
	"context"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

var log = logrus.WithField("pkg", "league-schedule")

type (
	Scheduler struct {
		ctx       context.Context
		ctxCancel context.CancelFunc
		ticker    *time.Ticker

		leagueRepo    LeagueRepo
		challengeRepo ChallengeRepo
	}

	LeagueRepo interface {
		ListLeagues(query interface{}, offset, limit int) (leagues []*model.League, err error)
		UpdateLeague(id string, updater interface{}) (updated *model.League, err error)
	}

	ChallengeRepo interface {
		GetChallenge(id string) (dc *model.DevChallenge, err error)
		UpdateChallenge(id string, updater interface{}) (dc *model.DevChallenge, err error)
	}
)

func NewLeagueScheduler(leagueRepo LeagueRepo, challengeRepo ChallengeRepo) *Scheduler {
	ctx, ctxCancel := context.WithCancel(context.Background())
	return &Scheduler{
		ctx:           ctx,
		ctxCancel:     ctxCancel,
		ticker:        time.NewTicker(time.Minute),
		leagueRepo:    leagueRepo,
		challengeRepo: challengeRepo,
	}
}

func (s *Scheduler) Run() {
	go func() {
		log.Infof("background scheduler started")
		defer log.Infof("background scheduler stopped")

		for {
			select {
			case <-s.ctx.Done():
				return
			case t := <-s.ticker.C:
				log.Tracef("check league at %s", t.Format(time.RFC3339))
				if err := s.startUpcomingLeagues(t); err != nil {
					log.Errorf("failed to start upcoming league(s): %v", err)
				}
				if err := s.stopFinishedLeagues(t); err != nil {
					log.Errorf("failed to stop finished league(s): %v", err)
				}
			}
		}
	}()
}

func (s *Scheduler) Close() error {
	s.ticker.Stop()
	s.ctxCancel()
	return nil
}

func (s *Scheduler) startUpcomingLeagues(now time.Time) error {
	query := bson.M{
		"isActive": false,
		"start":    bson.M{"$lte": now.Unix()},
		"end":      bson.M{"$gt": now.Unix()},
	}
	leagues, err := s.leagueRepo.ListLeagues(query, 0, config.DefaultPageSize)
	if err != nil {
		return errors.Wrapf(err, "failed to get upcoming leagues")
	}

	for _, league := range leagues {
		if err := s.enableLeague(league, now); err != nil {
			return errors.Wrapf(err, "failed to enable league")
		}
	}
	return nil
}

func (s *Scheduler) enableLeague(league *model.League, now time.Time) error {
	// Change all league's challenge status to active
	// TODO: Can use UpdateAll
	for _, cid := range league.Challenges {
		updater := bson.M{
			"disabled":   false,
			"updated_at": now,
		}
		_, err := s.challengeRepo.UpdateChallenge(cid, updater)
		if err != nil {
			return errors.Wrapf(err, "failed to update challenge")
		}
		log.Infof("league challenge status set to active: %v", cid)
	}

	// Enable league
	_, err := s.leagueRepo.UpdateLeague(league.ID, bson.M{"isActive": true, "updated_at": now})
	if err != nil {
		return errors.Wrapf(err, "failed to change league status to active")
	}
	log.Infof("league enabled: %v", league.Title)
	return nil
}

func (s *Scheduler) stopFinishedLeagues(now time.Time) error {
	query := bson.M{
		"isActive": true,
		"start":    bson.M{"$lt": now.Unix()},
		"end":      bson.M{"$lte": now.Unix()},
	}
	leagues, err := s.leagueRepo.ListLeagues(query, 0, config.DefaultPageSize)
	if err != nil {
		return errors.Wrapf(err, "failed to get finished leagues")
	}

	for _, league := range leagues {
		if err := s.disableLeague(league, now); err != nil {
			return errors.Wrapf(err, "failed to disable league")
		}
	}
	return nil
}

func (s *Scheduler) disableLeague(league *model.League, now time.Time) error {
	// Remove league from challenge
	for _, cid := range league.Challenges {
		challenge, err := s.challengeRepo.GetChallenge(cid)
		if err != nil {
			log.Errorf("failed to find challenge: %v", err)
			continue
		}

		idx := findIdx(challenge.Leagues, league.ID)
		if idx == -1 {
			continue
		}
		challenge.Leagues = append(challenge.Leagues[:idx], challenge.Leagues[idx+1:]...)

		_, err = s.challengeRepo.UpdateChallenge(cid, bson.M{"leagues": challenge.Leagues, "updated_at": now.Unix()})
		if err != nil {
			log.Errorf("failed to remove league from challenge: %v", err)
			continue
		}
	}

	// Change league status to finished
	_, err := s.leagueRepo.UpdateLeague(league.ID, bson.M{"isActive": false, "updated_at": now})
	if err != nil {
		return errors.Wrapf(err, "failed to change league status to finished")
	}
	log.Infof("league disabled: %v", league.Title)
	return nil
}

func findIdx(leagues []string, lid string) int {
	for i := 0; i < len(leagues); i++ {
		if leagues[i] == lid {
			return i
		}
	}
	return -1
}
