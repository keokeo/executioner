package league

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/pkg/util"
	"gitlab.com/fighters-solution/executioner/pkg/util/request"
	"gitlab.com/fighters-solution/executioner/pkg/util/response"
)

var (
	log = logrus.WithField("pkg", "league")
)

type (
	Service struct {
		leagueRepo Repo
	}

	Repo interface {
		CreateLeague(league *model.League) (created *model.League, err error)
		GetLeague(id string) (league *model.League, err error)
		ListLeagues(query interface{}, offset, limit int) (leagues []*model.League, err error)
		UpdateLeague(id string, updater interface{}) (updated *model.League, err error)
		DeleteLeague(id string) (err error)
		GetLeagueNames(lids []string) (lnames []leagueName, err error)
	}
)

func NewService(leagueRepo Repo) *Service {
	return &Service{
		leagueRepo: leagueRepo,
	}
}

func (s *Service) CreateLeague(w http.ResponseWriter, r *http.Request) {
	l := model.League{}
	if err := json.NewDecoder(r.Body).Decode(&l); err != nil {
		log.Errorf("failed to decode create API body to league: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	// TODO: Validate

	l.ID = util.UUID()
	now := time.Now().Unix()
	l.CreatedAt = now
	l.UpdatedAt = now
	created, err := s.leagueRepo.CreateLeague(&l)
	if err != nil {
		log.Errorf("failed to save league: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, created)
}

func (s *Service) GetLeague(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "lid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	u, err := s.leagueRepo.GetLeague(id)
	if err != nil {
		log.Errorf("failed to get league: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, u)
}

func (s *Service) ListLeagues(w http.ResponseWriter, r *http.Request) {
	offset := request.QueryInt(r, "offset")
	limit := config.GetPageSize(request.QueryInt(r, "limit"))
	timeRange := request.Query(r, "t")

	var query interface{}
	now := time.Now().Unix()
	switch timeRange {
	case "future":
		query = bson.M{"end": bson.M{"$gt": now}}
	case "past":
		query = bson.M{"end": bson.M{"$lt": now}}
	default:
		query = bson.M{}
	}

	dcs, err := s.leagueRepo.ListLeagues(query, offset, limit)
	if err != nil {
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, dcs)
}

func (s *Service) UpdateLeague(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "lid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	updater := make(map[string]interface{})
	err := json.NewDecoder(r.Body).Decode(&updater)
	if err != nil {
		log.Errorf("failed to decode update API to league: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	delete(updater, "id")
	updater["updatedAt"] = time.Now().Unix()
	updated, err := s.leagueRepo.UpdateLeague(id, updater)
	if err != nil {
		log.Errorf("failed to update league: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, updated)
}

func (s *Service) DeleteLeague(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "lid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if err := s.leagueRepo.DeleteLeague(id); err != nil {
		log.Errorf("failed to delete league: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, "")
}

func (s *Service) ResolveLeagueName(w http.ResponseWriter, r *http.Request) {
	lids := make([]string, 0)
	if err := json.NewDecoder(r.Body).Decode(&lids); err != nil {
		log.Errorf("failed to decode body to list of league IDs: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	if len(lids) == 0 {
		response.JSON(w, http.StatusOK, "")
		return
	}

	lnames, err := s.leagueRepo.GetLeagueNames(lids)
	if err != nil {
		log.Errorf("failed to resolve league names: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, lnames)
}
