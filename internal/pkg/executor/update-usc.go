package executor

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/pkg/errors"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/pkg/util"
)

func (s *Service) updateUserSolvedChallenge(uid string, us *UserSolution, outResp *OutputResponse) (updateMeta UpdateMeta, err error) {
	challenge, err := s.devChallengeRepo.GetChallenge(us.CID)
	if err != nil {
		return UpdateMeta{}, errors.Wrap(err, "failed to get challenge")
	}
	if challenge.Disabled {
		log.Infof("Challenge is disabled. Skip updating user solved challenge")
		return UpdateMeta{}, nil
	}

	isFailed := outResp.IsFailed
	usc, err := s.uscRepo.GetUserSolvedChallenge(uid, us.CID)
	if err != nil && err != mgo.ErrNotFound {
		return UpdateMeta{}, errors.Wrapf(err, "failed to get user solved challenges")
	}

	// First time submit
	now := time.Now().Unix()
	if err == mgo.ErrNotFound || usc == nil {
		usc, err = s.uscRepo.CreateUserSolvedChallenge(&model.UserSolvedChallenge{
			ID:        util.UUID(),
			UID:       uid,
			CID:       us.CID,
			Versions:  []model.Version{},
			CreatedAt: now,
			UpdatedAt: now,
		})
		if err != nil {
			return UpdateMeta{}, errors.Wrap(err, "failed to create user solved challenge")
		}
	}

	updater := bson.M{}
	uscID := usc.ID
	usc.Versions = append(usc.Versions, model.Version{
		SubmittedAt: now,
		Language:    util.UppercaseFirstChar(us.Language),
		IsSolved:    !isFailed,
		ExecTime:    outResp.ExecTime,
		Code:        us.Code,
	})
	updater["versions"] = usc.Versions

	// Solution failed -> Only update the submitted history
	if isFailed {
		if _, err := s.uscRepo.UpdateUserSolvedChallenge(uscID, updater); err != nil {
			return UpdateMeta{}, errors.Wrapf(err, "failed to update user solved challenge's submit version")
		}
		return UpdateMeta{}, nil
	}

	// Solution passed
	updater["isSolved"] = true
	execTimeDelta := 0.0
	if usc.FastestRun == 0 || usc.FastestRun > outResp.ExecTime { // Fastest solution
		execTimeDelta = outResp.ExecTime - usc.FastestRun
		usc.FastestRun = outResp.ExecTime
		usc.SolvedAt = now
		usc.Language = util.UppercaseFirstChar(us.Language)

		updater["fastestRun"] = usc.FastestRun
		updater["solvedAt"] = usc.SolvedAt
		updater["language"] = usc.Language
	}

	if usc.IsSolved { // User already solved -> Only update fastest run and submitted history
		_, err := s.uscRepo.UpdateUserSolvedChallenge(uscID, updater)
		if err != nil {
			return UpdateMeta{}, errors.Wrap(err, "failed to update user solved challenge")
		}

		// This challenge is running in league(s) so update league stats, too
		if len(challenge.Leagues) > 0 {
			err := s.updateLeagueStats(challenge.Leagues, uid, false, 0, now, execTimeDelta)
			if err != nil {
				return UpdateMeta{}, errors.Wrap(err, "failed to update leagues statistics")
			}
		}
		return UpdateMeta{
			IsUserUpdated: true,
		}, nil
	}

	// First time solved
	usc.IsSolved = true
	updateMeta.IsFirstSolved = true
	usc.Point = challenge.Point
	updater["isSolved"] = usc.IsSolved
	updater["point"] = usc.Point
	// Done update user solved challenge
	if _, err = s.uscRepo.UpdateUserSolvedChallenge(uscID, updater); err != nil {
		return UpdateMeta{}, errors.Wrap(err, "failed to update user solved challenge")
	}

	if len(challenge.Leagues) > 0 { // This challenge is running in league(s) so update league stats, too
		err := s.updateLeagueStats(challenge.Leagues, uid, true, challenge.Point, now, execTimeDelta)
		if err != nil {
			return UpdateMeta{}, errors.Wrap(err, "failed to update leagues statistics")
		}
	}

	// Update user points
	user, err := s.userRepo.GetUser(uid)
	if err != nil {
		return UpdateMeta{}, errors.Wrap(err, "failed to get user")
	}
	updateMeta.UserOldXP = user.Cf.Xp
	user.Cf.Xp += challenge.Point
	updateMeta.UserCurrentXP = user.Cf.Xp
	user.Cf.Solved++
	user.Cf.UpdatedAt = now

	_, err = s.userRepo.UpdateUser(uid, bson.M{"cf": user.Cf})
	if err != nil {
		return UpdateMeta{}, errors.Wrap(err, "failed to update user statistics")
	}

	// Update challenge's solved number
	challenge.SolvedBy++
	_, err = s.devChallengeRepo.UpdateChallenge(challenge.ID, bson.M{"solvedBy": challenge.SolvedBy, "updatedAt": now})
	if err != nil {
		return UpdateMeta{}, errors.Wrap(err, "failed to update challenge solved number")
	}

	updateMeta.IsUserUpdated = true
	return updateMeta, nil
}

func (s *Service) updateLeagueStats(leagueIDs []string, uid string, isFirstSolved bool, point int, solvedAt int64, execDeltaTime float64) error {
	for _, lid := range leagueIDs {
		league, err := s.leagueRepo.GetLeague(lid)
		if err != nil {
			return errors.Wrap(err, "failed to get league")
		}

		// Default competitor at first solved
		competitor := model.Competitor{
			UID:           uid,
			Point:         point,
			Solved:        1,
			LastSolved:    solvedAt,
			TotalExecTime: execDeltaTime,
		}

		compIdx := isCompetitorExists(league.Competitors, uid)
		if compIdx != -1 { // Competitor existed
			leagueComp := league.Competitors[compIdx]
			competitor.Point = leagueComp.Point
			competitor.Solved = leagueComp.Solved
			competitor.TotalExecTime = leagueComp.TotalExecTime + execDeltaTime

			if isFirstSolved { // First solved so update # of solved challenges and point
				competitor.Solved++
				competitor.Point += point
			}
			league.Competitors[compIdx] = competitor // Competitor existed so only update previous stats
		} else { // Competitor not existed yet, so append new one
			league.Competitors = append(league.Competitors, competitor)
		}

		_, err = s.leagueRepo.UpdateLeague(lid, bson.M{"competitors": league.Competitors})
		if err != nil {
			return errors.Wrap(err, "failed to update league competitor stats")
		}
	}

	return nil
}

func isCompetitorExists(comps []model.Competitor, uid string) int {
	for i, comp := range comps {
		if comp.UID == uid {
			return i
		}
	}
	return -1
}
