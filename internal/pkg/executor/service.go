package executor

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/pkg/util"
	"gitlab.com/fighters-solution/executioner/pkg/util/response"
)

const (
	docker = "docker"
)

var (
	log = logrus.WithField("pkg", "executor")

	passedRegex = regexp.MustCompile(`<PASSED::>`)
	failedRegex = regexp.MustCompile(`<FAILED::>`)
	errorRegex  = regexp.MustCompile(`<ERROR::>`)

	completeInRegex = regexp.MustCompile(`<COMPLETEDIN::>(\d+.?\d*)`)
	execTimeRegex   = regexp.MustCompile(`<EXECTIME::>(\d+.?\d*)`)

	bannedWords = []string{"<DESCRIBE::>", "<IT::>", "<PASSED::>", "<FAILED::>",
		"<ERROR::>", "<COMPLETEDIN::>", "<EXECTIME::>", "<TAB::", "<:LF:>"}
)

type (
	Service struct {
		conf config.Runner
		mux  *sync.RWMutex

		devChallengeRepo Repo
		uscRepo          UserSolvedChallengeRepo
		userRepo         UserRepo
		leagueRepo       LeagueRepo
	}

	Repo interface {
		GetChallenge(cid string) (dc *model.DevChallenge, err error)
		UpdateChallenge(id string, updater interface{}) (dc *model.DevChallenge, err error)
	}

	UserSolvedChallengeRepo interface {
		CreateUserSolvedChallenge(usc *model.UserSolvedChallenge) (created *model.UserSolvedChallenge, err error)
		GetUserSolvedChallenge(uid, cid string) (usc *model.UserSolvedChallenge, err error)
		UpdateUserSolvedChallenge(id string, updater interface{}) (usc *model.UserSolvedChallenge, err error)
	}

	UserRepo interface {
		GetUser(uid string) (u *model.User, err error)
		UpdateUser(id string, updater interface{}) (u *model.User, err error)
	}

	LeagueRepo interface {
		GetLeague(lid string) (l *model.League, err error)
		UpdateLeague(id string, updater interface{}) (l *model.League, err error)
	}

	UserSolution struct {
		CID      string `json:"cid"`
		Code     string `json:"code"`
		Language string `json:"language"`
	}

	OutputResponse struct {
		Stdout     string     `json:"stdout"`
		Stderr     string     `json:"stderr"`
		IsFailed   bool       `json:"failed"`
		ExecTime   float64    `json:"execTime"`
		UpdateMeta UpdateMeta `json:"meta"`
	}

	UpdateMeta struct {
		IsFirstSolved bool `json:"isFirstSolved"`
		IsUserUpdated bool `json:"isUserUpdated"`
		UserOldXP     int  `json:"userOldXP"`
		UserCurrentXP int  `json:"userCurrentXP"`
	}
)

func (us *UserSolution) validate() error {
	if us.CID == "" {
		return errors.New("invalid challenge ID")
	}
	if us.Language == "" {
		return errors.New("invalid language")
	}
	if getRunner(us.Language) == nil {
		return errors.Errorf("language not supported: %s", us.Language)
	}
	if us.Code == "" {
		return errors.New("invalid solution")
	}

	for _, bw := range bannedWords {
		if strings.Contains(us.Code, bw) {
			return fmt.Errorf("solution contains banned text: %s", bw)
		}
	}
	return nil
}

func NewService(conf config.Runner, devChallengeRepo Repo, uscRepo UserSolvedChallengeRepo, userRepo UserRepo, leagueRepo LeagueRepo) *Service {
	return &Service{
		conf:             conf,
		mux:              &sync.RWMutex{},
		devChallengeRepo: devChallengeRepo,
		uscRepo:          uscRepo,
		userRepo:         userRepo,
		leagueRepo:       leagueRepo,
	}
}

func (s *Service) ConfigChanged(conf config.Config) error {
	s.mux.Lock()
	defer s.mux.Unlock()
	s.conf = conf.Runner
	log.Debugf("runner config updated")
	return nil
}

func (s *Service) RunTest(w http.ResponseWriter, r *http.Request) {
	us := UserSolution{}
	if err := json.NewDecoder(r.Body).Decode(&us); err != nil {
		log.Errorf("failed to decode test request body to user solution: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if err := us.validate(); err != nil {
		log.Errorf("user solution validation failed: %v", err)
		response.Error(w, err, http.StatusBadRequest)
		return
	}

	dockerParams, err := s.buildDockerRunParams(&us, true)
	if err != nil {
		log.Errorf("failed to build docker command params: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	output, err := execRunner(docker, dockerParams)
	if err != nil {
		log.Errorf("failed to exec runner: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	log.Tracef("output: %v", util.ToJSONString(output))

	response.JSON(w, http.StatusOK, verifyRunnerOutput(w, &us, output))
}

func (s *Service) SubmitSolution(w http.ResponseWriter, r *http.Request) {
	uid := chi.URLParam(r, "uid")
	if uid == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	us := UserSolution{}
	if err := json.NewDecoder(r.Body).Decode(&us); err != nil {
		log.Errorf("failed to decode test request body to user solution: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if err := us.validate(); err != nil {
		log.Errorf("user solution validation failed: %v", err)
		response.Error(w, err, http.StatusBadRequest)
		return
	}

	dockerParams, err := s.buildDockerRunParams(&us, false)
	if err != nil {
		log.Errorf("failed to build docker command params: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	output, err := execRunner(docker, dockerParams)
	if err != nil {
		log.Errorf("failed to exec runner: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	log.Tracef("output: %v", util.ToJSONString(output))

	outResp := verifyRunnerOutput(w, &us, output)
	updateMeta, err := s.updateUserSolvedChallenge(uid, &us, outResp)
	if err != nil {
		log.Errorf("failed to update user solved challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	outResp.UpdateMeta = updateMeta
	response.JSON(w, http.StatusOK, outResp)
}

func verifyRunnerOutput(w http.ResponseWriter, us *UserSolution, out *model.RunnerOutput) *OutputResponse {
	execTime := parseExecutionTime(out.Stdout, model.Language(us.Language))
	log.Tracef("execTime: %.4fs", execTime)

	var (
		cntPassed = len(passedRegex.FindAllString(out.Stdout, -1))
		cntFailed = len(failedRegex.FindAllString(out.Stdout, -1))
		cntError  = len(errorRegex.FindAllString(out.Stdout, -1))
	)

	outResp := &OutputResponse{
		ExecTime: execTime,
	}
	if out.Stderr != "" && cntFailed == 0 { // Build failed
		outResp.IsFailed = true
		// Prevent throw exception to get test case
		if model.Language(us.Language) == model.LanguagePython3 && strings.Contains(out.Stderr, "Exception") {
			outResp.Stderr = "Test has exception but omitted"
			return outResp
		}
		outResp.Stderr = out.Stderr
		return outResp
	}

	if cntError > 0 && cntFailed == 0 && cntPassed == 0 { // Syntax error
		outResp.IsFailed = true
		outResp.Stderr = parseTestStatus(out.Stdout, model.Language(us.Language))
		return outResp
	}

	// Runner failed (e.g.: timeout)
	if out.ExitSignal != "" || out.Status != "" {
		header := "RUNNER FAILED TO EXECUTE YOUR SOLUTION"
		outResp.IsFailed = true
		lines := []string{
			header,
			strings.Repeat("-", len(header)),
			"",
			getRunnerStatus(out, outResp),
		}
		outResp.Stderr = strings.Join(lines, "\n")
		return outResp
	}

	cntFailed += cntError
	// Solution failed
	if out.ExitCode != 0 || cntFailed > 0 || cntPassed == 0 {
		header := fmt.Sprintf("%d PASSED, %d FAILED!", cntPassed, cntFailed)
		outResp.IsFailed = true
		lines := []string{
			header,
			strings.Repeat("-", len(header)),
			"",
			parseTestStatus(out.Stdout, model.Language(us.Language)),
			strings.Repeat("-", len(header)),
			"",
			getRunnerStatus(out, outResp),
		}
		outResp.Stderr = strings.Join(lines, "\n")
		return outResp
	}

	// Passed
	header := fmt.Sprintf("ALL %d TEST CASES PASSED!", cntPassed)
	lines := []string{
		header,
		strings.Repeat("-", len(header)),
		"",
		parseTestStatus(out.Stdout, model.Language(us.Language)),
		strings.Repeat("-", len(header)),
		"",
		getRunnerStatus(out, outResp),
	}
	outResp.Stdout = strings.Join(lines, "\n")
	outResp.IsFailed = false
	return outResp
}

func parseExecutionTime(in string, lang model.Language) float64 {
	if in == "" {
		return 0
	}

	totalTime, lastExecTime := 0.0, 0.0
	var err error
	matchedGroups := completeInRegex.FindAllStringSubmatch(in, -1)
	for _, group := range matchedGroups {
		lastExecTime, err = strconv.ParseFloat(group[1], 64)
		if err != nil {
			log.Errorf("failed to parse completeIn string to float: %v", err)
			continue
		}
		totalTime += lastExecTime
	}

	if totalTime > 0 {
		// Kotlin also report the total CompleteIn for the whole class test at the end
		// of output.
		// So we  have to subtract that value to get the actual execTime.
		if lang == model.LanguageKotlin {
			return (totalTime - lastExecTime) / 1000 // ms to sec
		}
		if lang == model.LanguageJavascript {
			totalTime /= 1000
		}
		return totalTime
	}

	// CompletedIn was not reported so parse execTime
	totalTime = 0
	matchedGroups = execTimeRegex.FindAllStringSubmatch(in, -1)
	for _, group := range matchedGroups {
		lastExecTime, err = strconv.ParseFloat(group[1], 64)
		if err != nil {
			log.Errorf("failed to parse execTime string to float: %v", err)
			continue
		}
		totalTime += lastExecTime
	}

	if lang == model.LanguageKotlin || lang == model.LanguageJavascript {
		return totalTime / 1000 // ms to sec
	}
	return totalTime
}

func parseTestStatus(out string, lang model.Language) string {
	spl := strings.Split(out, "\n")
	ret := ""
	itCount := 0

	isException := strings.Contains(out, "java.lang.RuntimeException")

	for _, line := range spl {
		switch {
		case strings.HasPrefix(line, "<DESCRIBE::>"):
			ret += fmt.Sprintf("TEST SUITE: %s\n", line[12:])
		case strings.HasPrefix(line, "<IT::>"):
			itCount++
			ret += fmt.Sprintf("%3d. %s\n", itCount, line[6:])
		case strings.HasPrefix(line, "<TAB::Stack Trace>"): // Go panic
			line = strings.ReplaceAll(string(line[18:]), "<:LF:>", "\n")
			ret += "      " + line + "\n"
		case strings.HasPrefix(line, "<PASSED::>"):
			ret += "      ✓ PASSED\n"
		case strings.HasPrefix(line, "<FAILED::>"):
			ret += "      ✗ FAILED"
			if lang == model.LanguageKotlin || lang == model.LanguageJava {
				if isException {
					ret += ": RuntimeException"
				} else {
					exIdx := strings.Index(line, "expected:")
					if exIdx == -1 {
						exIdx = len(line)
					}
					ret += fmt.Sprintf(": %s", strings.ReplaceAll(line[10:exIdx], "<:LF:>", " - "))
				}
			}
			ret += "\n"
		case strings.HasPrefix(line, "<ERROR::>"):
			lfIdx := strings.Index(line, "<:LF:>")
			if lfIdx == -1 {
				lfIdx = len(line)
			}
			line = line[9:lfIdx]
			// Omit log as it's possible user trying to throw error to get test case
			if lang == model.LanguageJavascript && strings.HasPrefix(strings.TrimSpace(line), "Error") {
				ret += "      ☢ ERROR: Test has error but omitted\n"
				continue
			}
			ret += fmt.Sprintf("      ☢ ERROR: %s\n", line)
		}
	}

	return ret
}

func getRunnerStatus(out *model.RunnerOutput, outResp *OutputResponse) string {
	execTime := "-"
	if outResp.ExecTime > 0 {
		execTime = fmt.Sprintf("%.4fms", outResp.ExecTime*1000)
	}
	if out.Status == "" || out.Status == "null" {
		out.Status = "ok"
		if outResp.IsFailed {
			out.Status = "-"
		}
	}
	if out.ExitSignal == "" {
		out.ExitSignal = "-"
	}
	// wallTime := "-"
	// if out.WallTime > 0 {
	// 	wallTime = fmt.Sprintf("%dms", out.WallTime)
	// }
	ret := []string{
		"RUNNER STATUS",
		fmt.Sprintf("IsFailed: %t", outResp.IsFailed),
		fmt.Sprintf("ExitCode: %v", out.ExitCode),
		fmt.Sprintf("ExitSignal: %v", out.ExitSignal),
		fmt.Sprintf("Status: %v", out.Status),
		// fmt.Sprintf("WallTime: %s", wallTime),
		fmt.Sprintf("ExecTime: %s", execTime),
	}
	return strings.Join(ret, "\n")
}
