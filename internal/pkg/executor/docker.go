package executor

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os/exec"
	"strconv"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

type runner struct {
	language      string
	runner        string
	testFramework string
}

var runners = []runner{
	{
		language:      "javascript",
		runner:        "codewars/node-runner",
		testFramework: "cw-2",
	},
	{
		language:      "kotlin",
		runner:        "lnquy/kotlin-runner",
		testFramework: "kotlintest",
	},
	{
		language:      "go",
		runner:        "codewars/go-runner",
		testFramework: "ginkgo",
	},
	{
		language:      "python",
		runner:        "codewars/python-runner",
		testFramework: "cw-2",
	},
	{
		language:      "java",
		runner:        "codewars/java-runner",
		testFramework: "junit4",
	},
	{
		language:      "python3",
		runner:        "codewars/python-runner",
		testFramework: "cw-2",
	},
	{
		language:      "c",
		runner:        "codewars/systems-runner",
		testFramework: "criterion",
	},
	{
		language:      "cpp",
		runner:        "codewars/systems-runner",
		testFramework: "igloo",
	},
}

func getRunner(language string) *runner {
	for _, v := range runners {
		if v.language == language {
			return &v
		}
	}
	return nil
}

// TODO: validate solution
func (s *Service) buildDockerRunParams(solution *UserSolution, isTestOnly bool) (params []string, err error) {
	run := getRunner(solution.Language)
	if run == nil {
		return nil, errors.Errorf("language not found: %v", solution.Language)
	}

	dc, err := s.devChallengeRepo.GetChallenge(solution.CID)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to get dev challenge")
	}

	getTestData := func(codes []model.Code, lang string, isTestOnly bool) (string, int) {
		for _, code := range codes {
			if code.Language == lang {
				if isTestOnly {
					return code.Fixture, int(code.Timeout)
				}
				return code.TestCase, int(code.Timeout)
			}
		}
		return "", 0
	}

	testData, timeout := getTestData(dc.Codes, solution.Language, isTestOnly)
	if timeout == 0 {
		timeout = config.DefaultRunnerTimeout
	}

	// Note: Minimum spec for Gradle/JVM based runner is 2CPUs, 1024MB mem/swap
	// This is not fair cause Java and Kotlin runenr has more resource than other
	// languages, but let's accept it for now :(
	cpu, mem, memSwap := s.conf.CPU, s.conf.MemoryMB, s.conf.MemorySwapMB
	if run.language == string(model.LanguageJava) || run.language == string(model.LanguageKotlin) {
		if cpu < 2 {
			cpu = 2
		}
		if mem < 1024 {
			mem = 1024
		}
		if memSwap < 1024 {
			memSwap = 1024
		}
	}
	return []string{
		"run",
		"--network", "none",
		"--cpus", strconv.Itoa(cpu),
		"--memory", fmt.Sprintf("%dm", mem),
		"--memory-swap", fmt.Sprintf("%dm", memSwap),
		"--rm", run.runner,
		"run", "--",
		"--language", run.language,
		"--test-framework", run.testFramework,
		"--code", solution.Code,
		"--fixture", testData,
		"--format", "json",
		"--timeout", strconv.Itoa(timeout),
	}, nil
}

// TODO: Context
func execRunner(appCmd string, params []string) (output *model.RunnerOutput, err error) {
	log.Tracef("%s %s", appCmd, strings.Join(params, " "))
	cmd := exec.Command(appCmd, params...)
	var outBuff bytes.Buffer
	cmd.Stdout = &outBuff

	if err := cmd.Run(); err != nil {
		log.Errorf("failed to execute runner command: %v", err)
		// Continue, try to parse runner output
	}

	output = &model.RunnerOutput{}
	if err := json.Unmarshal(outBuff.Bytes(), output); err != nil {
		return nil, errors.Wrapf(err, "failed to decode runner JSON output to object")
	}
	return output, nil
}
