package user_solved_challenge

import (
	"strconv"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/pkg/errors"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

const (
	userSolvedChallenges = "user_solved_challenges" // Collection name
)

type MongoDBRepo struct {
	session *mgo.Session
}

func NewMongoDBRepo(session *mgo.Session) *MongoDBRepo {
	return &MongoDBRepo{
		session: session,
	}
}

func (r *MongoDBRepo) CreateUserSolvedChallenge(usc *model.UserSolvedChallenge) (created *model.UserSolvedChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	if err := sess.DB("").C(userSolvedChallenges).Insert(usc); err != nil {
		return nil, err
	}
	return r.GetUserSolvedChallenge(usc.UID, usc.CID)
}

func (r *MongoDBRepo) GetUserSolvedChallenge(uid, cid string) (usc *model.UserSolvedChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	usc = &model.UserSolvedChallenge{}
	err = sess.DB("").C(userSolvedChallenges).Find(bson.M{"$and": []bson.M{{"uid": uid}, {"cid": cid}}}).One(usc)
	if err != nil {
		return nil, err
	}
	return usc, nil
}

func (r *MongoDBRepo) GetUserSolvedChallengeByID(id string) (usc *model.UserSolvedChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	usc = &model.UserSolvedChallenge{}
	err = sess.DB("").C(userSolvedChallenges).Find(bson.M{"id": id}).One(usc)
	if err != nil {
		return nil, err
	}
	return usc, nil
}

func (r *MongoDBRepo) GetSolution(uid, cid, sid string) (solution *model.Version, err error) {
	ts, err := strconv.ParseInt(sid, 10, 64)
	if err != nil {
		return nil, errors.Wrapf(err, "invalid solution ID")
	}

	usc, err := r.GetUserSolvedChallenge(uid, cid)
	if err != nil {
		return nil, err
	}

	for _, ver := range usc.Versions {
		if ver.SubmittedAt == ts {
			return &ver, nil
		}
	}
	return nil, mgo.ErrNotFound
}

func (r *MongoDBRepo) ListSolvedChallengesByUserID(uid string, offset, limit int) (uscs []*model.UserSolvedChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	uscs = make([]*model.UserSolvedChallenge, 0)
	err = sess.DB("").C(userSolvedChallenges).Find(bson.M{"$and": []bson.M{{"uid": uid}, {"isSolved": true}}}).Sort("-updatedAt").Skip(offset).Limit(limit).All(&uscs)
	if err != nil {
		return nil, err
	}
	return uscs, nil
}

func (r *MongoDBRepo) UpdateUserSolvedChallenge(id string, updater interface{}) (usc *model.UserSolvedChallenge, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	err = sess.DB("").C(userSolvedChallenges).Update(bson.M{"id": id}, bson.M{"$set": updater})
	if err != nil {
		return nil, err
	}
	return r.GetUserSolvedChallengeByID(id)
}

func (r *MongoDBRepo) DeleteUserSolvedChallenge(uid, cid string) (err error) {
	sess := r.session.Clone()
	defer sess.Close()

	return sess.DB("").C(userSolvedChallenges).Remove(bson.M{"$and": []bson.M{{"uid": uid}, {"cid": cid}}})
}
