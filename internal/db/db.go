package db

import (
	"fmt"
	"strings"

	"github.com/globalsign/mgo"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/pkg/health"
)

const (
	MongoDB = "mongodb"
)

var log = logrus.WithField("pkg", "db")

type (
	DbConn struct {
		Type        string
		MongoDBConn MongoDBConn
		// Add more db connection here later (e.g.: sql.DB) if we want to support another database
	}

	MongoDBConn struct {
		Config  config.MongoDB
		Session *mgo.Session
	}
)

func Init(conf config.Database) (*DbConn, error) {
	var dbConn DbConn

	switch strings.ToLower(conf.Type) {
	case MongoDB:
		dbConn = DbConn{
			Type: MongoDB,
			MongoDBConn: MongoDBConn{
				Config: conf.MongoDB,
			},
		}

		ms, err := mgo.DialWithInfo(&mgo.DialInfo{
			Addrs:    conf.MongoDB.Addresses,
			Username: conf.MongoDB.Username,
			Password: conf.MongoDB.Password,
			Database: conf.MongoDB.Database,
			Timeout:  conf.MongoDB.Timeout,
		})
		if err != nil {
			return nil, errors.Wrapf(err, "failed to dial MongoDB")
		}
		ms.SetMode(mgo.Monotonic, true)
		// Do further setup here if necessary
		dbConn.MongoDBConn.Session = ms
	default:
		return nil, fmt.Errorf("db type not implemented yet: %s", conf.Type)
	}

	return &dbConn, nil
}

func (d *DbConn) Close() error {
	switch d.Type {
	case MongoDB:
		if d.MongoDBConn.Session != nil {
			d.MongoDBConn.Session.Close()
		}
		log.Infof("db: MongoDB session closed")
	default:
		return fmt.Errorf("db type not implemented yet: %s", d.Type)
	}
	return nil
}

func (d *DbConn) Ping() []*health.Endpoint {
	var eps []*health.Endpoint

	switch d.Type {
	case MongoDB:
		mongoEp := &health.Endpoint{
			Name:      "mongodb",
			Addresses: d.MongoDBConn.Config.Addresses,
			Status:    health.OK,
		}
		if d.MongoDBConn.Session == nil || d.MongoDBConn.Session.Ping() != nil {
			mongoEp.Status = health.UNHEALTHY
		}
		eps = append(eps, mongoEp)
	}

	return eps
}
