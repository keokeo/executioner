package api

import (
	"io"
	"net/http"
	"path"

	"github.com/go-chi/chi"
	"github.com/gorilla/sessions"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/db"
	"gitlab.com/fighters-solution/executioner/internal/pkg/cache"
	"gitlab.com/fighters-solution/executioner/internal/pkg/dev-challenge"
	"gitlab.com/fighters-solution/executioner/internal/pkg/email"
	"gitlab.com/fighters-solution/executioner/internal/pkg/executor"
	"gitlab.com/fighters-solution/executioner/internal/pkg/leaderboard"
	"gitlab.com/fighters-solution/executioner/internal/pkg/league"
	"gitlab.com/fighters-solution/executioner/internal/pkg/league/schedule"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/internal/pkg/user"
	"gitlab.com/fighters-solution/executioner/internal/pkg/user-avatar"
	"gitlab.com/fighters-solution/executioner/internal/pkg/user-solved-challenge"
	"gitlab.com/fighters-solution/executioner/pkg/middleware"
	"gitlab.com/fighters-solution/executioner/pkg/middleware/auth"
)

var (
	log = logrus.WithField("pkg", "router")
)

type (
	routeGroup struct {
		name     string
		rootPath string
		routes   []route
	}

	route struct {
		name        string
		method      string
		path        string
		middlewares []middleware.Middleware
		handler     http.HandlerFunc
	}
)

func RegisterRoutes(conf *config.Config, mux *chi.Mux, dbConn *db.DbConn, cacheSvc *cache.Cache, emailSvc *email.Sender, sessStore sessions.Store) []io.Closer {
	var (
		devChallengeRepo dev_challenge.Repo
		userRepo         user.Repo
		avatarRepo       user.AvatarRepo
		uscRepo          user_solved_challenge.Repo
		leagueRepo       league.Repo
		leaderboardRepo  leaderboard.Repo
		authMdw          auth.Authenticator
	)

	switch dbConn.Type {
	case db.MongoDB:
		devChallengeRepo = dev_challenge.NewMongoDBRepo(dbConn.MongoDBConn.Session)
		userRepo = user.NewMongoDBRepo(dbConn.MongoDBConn.Session)
		avatarRepo = user_avatar.NewMongoDBRepo(dbConn.MongoDBConn.Session)
		uscRepo = user_solved_challenge.NewMongoDBRepo(dbConn.MongoDBConn.Session)
		leagueRepo = league.NewMongoDBRepo(dbConn.MongoDBConn.Session)
		leaderboardRepo = leaderboard.NewMongoDBRepo(dbConn.MongoDBConn.Session)
	default:
		log.Panicf("invalid db type: %v", dbConn.Type)
	}

	dcSvc := dev_challenge.NewService(devChallengeRepo)
	userSvc := user.NewService(userRepo,
		avatarRepo,
		cacheSvc,
		emailSvc,
		conf.Server.HostURL,
		conf.Misc.DefaultAvatar,
		conf.Server.Security.EnableEmailConfirm,
	)
	uscSvc := user_solved_challenge.NewService(uscRepo)
	leagueSvc := league.NewService(leagueRepo)
	leaderboardSvc := leaderboard.NewService(leaderboardRepo, leagueRepo)
	executorSvc := executor.NewService(conf.Runner, devChallengeRepo, uscRepo, userRepo, leagueRepo)
	authMdw = auth.NewAuthByCookie(sessStore)

	// Listen on config change
	conf.OnConfigChange(userSvc.ConfigChanged, executorSvc.ConfigChanged)

	apiV1 := getAPIv1Routes(dcSvc, userSvc, uscSvc, leagueSvc, leaderboardSvc, executorSvc, authMdw)

	log.Infof("registering APIv1 routes: /api/v1/*")
	for _, routeGroup := range apiV1 {
		log.Debugf("registering API group: %s - %s", routeGroup.name, routeGroup.rootPath)
		for _, route := range routeGroup.routes {
			handler := http.Handler(route.handler)
			for j := len(route.middlewares) - 1; j >= 0; j-- {
				handler = route.middlewares[j](handler)
			}

			urlPath := path.Join("/api/v1/", routeGroup.rootPath, route.path)
			mux.Method(route.method, urlPath, handler)
			log.Debugf("   > %s: %s %s", route.name, route.method, urlPath)
		}
	}

	// Start background workers
	scheduler := schedule.NewLeagueScheduler(leagueRepo, devChallengeRepo)
	scheduler.Run()
	return []io.Closer{scheduler}
}

func getAPIv1Routes(dcSvc *dev_challenge.Service,
	userSvc *user.Service,
	uscSvc *user_solved_challenge.Service,
	leagueSvc *league.Service,
	ldbSvc *leaderboard.Service,
	execSvc *executor.Service,
	authMdw auth.Authenticator) []routeGroup {

	return []routeGroup{
		{
			name:     "Dev challenge (programming)",
			rootPath: "/dev/challenges",
			routes: []route{
				{
					name:        "Create challenge",
					method:      http.MethodPost,
					path:        "/",
					middlewares: []middleware.Middleware{authMdw.Auth(model.NormalUser)},
					handler:     dcSvc.CreateChallenge,
				}, {
					name:        "Get challenge",
					method:      http.MethodGet,
					path:        "/{cid}",
					middlewares: []middleware.Middleware{},
					handler:     dcSvc.GetChallenge,
				}, {
					name:        "Get full challenge",
					method:      http.MethodGet,
					path:        "/f/{cid}/u/{uid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     dcSvc.GetFullChallenge,
				}, {
					name:        "List challenges",
					method:      http.MethodGet,
					path:        "/",
					middlewares: []middleware.Middleware{},
					handler:     dcSvc.ListChallenges,
				}, {
					name:        "Update challenges (user owner)",
					method:      http.MethodPatch,
					path:        "/{cid}/u/{uid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     dcSvc.UpdateChallenge,
				}, {
					name:        "Update challenges (admin)",
					method:      http.MethodPatch,
					path:        "/{cid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Admin)},
					handler:     dcSvc.UpdateChallenge,
				}, {
					name:        "Delete challenge",
					method:      http.MethodDelete,
					path:        "/{cid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Admin)},
					handler:     dcSvc.DeleteChallenge,
				}, {
					name:        "Resolve challenge names",
					method:      http.MethodPost,
					path:        "/resolve-name",
					middlewares: []middleware.Middleware{},
					handler:     dcSvc.ResolveChallengeName,
				},
			},
		},
		{
			name:     "User",
			rootPath: "/users",
			routes: []route{
				{
					name:        "Get user profile",
					method:      http.MethodGet,
					path:        "/{uid}",
					middlewares: []middleware.Middleware{},
					handler:     userSvc.GetUserProfile,
				}, {
					name:        "Update user profile",
					method:      http.MethodPatch,
					path:        "/{uid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     userSvc.UpdateUserProfile,
				}, {
					name:        "Update user settings",
					method:      http.MethodPatch,
					path:        "/{uid}/settings",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     userSvc.UpdateUserSettings,
				}, {
					name:        "Get user avatar",
					method:      http.MethodGet,
					path:        "/{uid}/avatar",
					middlewares: []middleware.Middleware{},
					handler:     userSvc.GetUserAvatar,
				}, {
					name:        "Set user avatar",
					method:      http.MethodPost,
					path:        "/{uid}/avatar",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     userSvc.SetUserAvatar,
				}, {
					name:        "Delete user avatar",
					method:      http.MethodDelete,
					path:        "/{uid}/avatar",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     userSvc.DeleteUserAvatar,
				}, {
					name:        "Resend confirmation email",
					method:      http.MethodGet,
					path:        "/{uid}/send-confirm-email",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     userSvc.SendConfirmEmail,
				},
			},
		},
		{
			name:     "User solved challenge",
			rootPath: "/usc",
			routes: []route{
				{
					name:        "Create user solved challenge",
					method:      http.MethodPost,
					path:        "/",
					middlewares: []middleware.Middleware{authMdw.Auth(model.NormalUser)},
					handler:     uscSvc.CreateUserSolvedChallenge,
				}, {
					name:        "Get user solved challenge",
					method:      http.MethodGet,
					path:        "/u/{uid}/c/{cid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Owner, model.Admin)},
					handler:     uscSvc.GetUserSolvedChallenge,
				}, {
					name:        "Get user solution",
					method:      http.MethodGet,
					path:        "/u/{uid}/c/{cid}/s/{sid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.NormalUser)},
					handler:     uscSvc.GetSolution,
				}, {
					name:        "List solved challenges by user",
					method:      http.MethodGet,
					path:        "/u/{uid}",
					middlewares: []middleware.Middleware{},
					handler:     uscSvc.ListSolvedChallengesByUserID,
				}, {
					name:        "Delete user solved challenge",
					method:      http.MethodDelete,
					path:        "/u/{uid}/c/{cid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Admin)},
					handler:     uscSvc.DeleteUserSolvedChallenge,
				},
			},
		},
		{
			name:     "League",
			rootPath: "/leagues",
			routes: []route{
				{
					name:        "Create league",
					method:      http.MethodPost,
					path:        "/",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Admin)},
					handler:     leagueSvc.CreateLeague,
				}, {
					name:        "Get league",
					method:      http.MethodGet,
					path:        "/{lid}",
					middlewares: []middleware.Middleware{},
					handler:     leagueSvc.GetLeague,
				}, {
					name:        "List all leagues",
					method:      http.MethodGet,
					path:        "/",
					middlewares: []middleware.Middleware{},
					handler:     leagueSvc.ListLeagues,
				}, {
					name:        "Update league",
					method:      http.MethodPatch,
					path:        "/{lid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Admin)},
					handler:     leagueSvc.UpdateLeague,
				}, {
					name:        "Delete league",
					method:      http.MethodDelete,
					path:        "/{lid}",
					middlewares: []middleware.Middleware{authMdw.Auth(model.Admin)},
					handler:     leagueSvc.DeleteLeague,
				}, {
					name:        "Resolve league's challenge names",
					method:      http.MethodPost,
					path:        "/resolve-name",
					middlewares: []middleware.Middleware{},
					handler:     leagueSvc.ResolveLeagueName,
				},
			},
		}, {
			name:     "Leaderboard",
			rootPath: "/leaderboard",
			routes: []route{
				{
					name:        "Get global leaderboard",
					method:      http.MethodGet,
					path:        "/",
					middlewares: []middleware.Middleware{},
					handler:     ldbSvc.GetGlobalLeaderboard,
				}, {
					name:        "Get challenge's leaderboard",
					method:      http.MethodGet,
					path:        "/c/{cid}",
					middlewares: []middleware.Middleware{},
					handler:     ldbSvc.GetChallengeLeaderboard,
				}, {
					name:        "Get league's leaderboard",
					method:      http.MethodGet,
					path:        "/l/{lid}",
					middlewares: []middleware.Middleware{},
					handler:     ldbSvc.GetLeagueLeaderboard,
				},
			},
		},
		{
			name:     "Executioner APIs",
			rootPath: "/exec",
			routes: []route{
				{
					name:        "Run tests",
					method:      http.MethodPost,
					path:        "/test",
					middlewares: []middleware.Middleware{authMdw.Auth(model.NormalUser)},
					handler:     execSvc.RunTest,
				}, {
					name:        "Submit solution",
					method:      http.MethodPost,
					path:        "/u/{uid}/submit",
					middlewares: []middleware.Middleware{authMdw.Auth(model.NormalUser)},
					handler:     execSvc.SubmitSolution,
				},
			},
		}, {
			name:     "Public APIs",
			rootPath: "/",
			routes: []route{
				{
					name:        "Create user",
					method:      http.MethodPost,
					path:        "/register",
					middlewares: []middleware.Middleware{},
					handler:     userSvc.Register,
				}, {
					name:        "Confirm user email",
					method:      http.MethodGet,
					path:        "/confirm",
					middlewares: []middleware.Middleware{},
					handler:     userSvc.ConfirmEmail,
				}, {
					name:        "Login",
					method:      http.MethodPost,
					path:        "/login",
					middlewares: []middleware.Middleware{},
					handler:     userSvc.Login,
				}, {
					name:        "Logout",
					method:      http.MethodPost,
					path:        "/logout",
					middlewares: []middleware.Middleware{},
					handler:     userSvc.Logout,
				},
			},
		},
	}
}
