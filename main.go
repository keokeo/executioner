package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/api"
	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/db"
	"gitlab.com/fighters-solution/executioner/internal/pkg/cache"
	"gitlab.com/fighters-solution/executioner/internal/pkg/email"
	"gitlab.com/fighters-solution/executioner/internal/session"
	"gitlab.com/fighters-solution/executioner/pkg/health"
	"gitlab.com/fighters-solution/executioner/pkg/middleware"
)

var (
	version = "" // Injected at build time
	log     = logrus.WithField("pkg", "main")

	fVersion    = flag.Bool("v", false, "Display app version")
	fHelp       = flag.Bool("h", false, "Display help")
	fStage      = flag.String("stage", "dev", "App running stage")
	fConfigPath = flag.String("config", "configs", "Path to configuration folder")
)

func main() {
	// App arguments
	flag.Parse()
	if *fHelp {
		flag.PrintDefaults()
		return
	}
	if *fVersion {
		fmt.Println(version)
		return
	}

	// Configuration
	stage := config.ParseAppStage(*fStage)
	log.Infof("running on %s stage", stage)
	conf, err := config.Read(stage, *fConfigPath)
	if err != nil {
		log.Panicf("failed to read config from file: %v", err)
	}

	// Logging
	var logWriter io.WriteCloser
	if conf.Log.FilePath != "" {
		lf, err := os.OpenFile(conf.Log.FilePath, os.O_WRONLY|os.O_CREATE, 0644)
		if err != nil {
			log.Panicf("failed to open log file: %v", err)
		}
		logWriter = io.WriteCloser(lf)
	}
	initLogger(conf.Log, logWriter)
	defer func() {
		log.Infof("application exit")
		if logWriter != nil {
			_ = logWriter.Close()
		}
	}()

	// Init dependencies
	// Database (MongoDB)
	dbConn, err := db.Init(conf.Database)
	if err != nil {
		log.Panicf("failed to init database connection: %v", err)
	}
	defer dbConn.Close()
	health.RegisterHealthEndpoints(dbConn)
	// Redis
	cacheSvc, err := cache.New(conf.Redis)
	if err != nil {
		log.Panicf("failed to init Redis connection: %v", err)
	}
	defer cacheSvc.Close()
	health.RegisterHealthEndpoints(cacheSvc)
	// Email
	emailSvc, err := email.NewSender(conf.Email)
	if err != nil {
		log.Panicf("failed to init SMTP connection: %v", err)
	}
	// Session
	sessStore := session.InitStore(conf.Server.Security.Session, conf.Redis)

	// Router
	mux := chi.NewRouter()
	if conf.Server.Security.EnableCORS {
		log.Warnf("CORS enabled. May not suitable for production environment")
		corsOpt := cors.New(cors.Options{
			AllowedOrigins:   []string{"*"},
			AllowCredentials: true,
			AllowedHeaders:   []string{"*"},
			AllowedMethods: []string{
				http.MethodGet,
				http.MethodPost,
				http.MethodPatch,
				http.MethodPut,
				http.MethodDelete,
				http.MethodOptions,
				http.MethodHead,
			},
		})
		mux.Use(corsOpt.Handler)
	}
	mux.Use(middleware.Recover, middleware.ExecutionTime)
	closers := api.RegisterRoutes(conf, mux, dbConn, cacheSvc, emailSvc, sessStore)
	health.RegisterRoutes(mux)

	// Server
	addr := fmt.Sprintf("%s:%s", conf.Server.Address, conf.Server.Port)
	server := http.Server{
		Addr:         addr,
		Handler:      mux,
		ReadTimeout:  conf.Server.ReadTimeout,
		WriteTimeout: conf.Server.WriteTimeout,
	}

	// Start server
	go func() {
		health.SetStatus(true, true) // Mark HTTP server as ready to serve requests

		// HTTPS
		if conf.Server.TLS.Enable {
			log.Infof("starting HTTPS server on: %s", addr)
			err := server.ListenAndServeTLS(conf.Server.TLS.CertFile, conf.Server.TLS.KeyFile)
			if err != nil && err != http.ErrServerClosed {
				log.Panicf("failed to start HTTPS server: %v", err)
			}
			return
		}

		// HTTP
		log.Infof("starting HTTP server on: %s", addr)
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Panicf("failed to start HTTP server: %v", err)
		}
	}()

	// Graceful shutdown
	stopChan := make(chan os.Signal, 1)
	signal.Notify(stopChan, syscall.SIGINT, syscall.SIGTERM)
	sig := <-stopChan
	log.Infof("%s signal received. exiting", sig)
	// Stop HTTP server first
	stopCtx, stopCtxCancel := context.WithTimeout(context.Background(), conf.Server.ShutdownTimeout)
	defer stopCtxCancel() // No meaning, just to prevent go vet complains
	if err := server.Shutdown(stopCtx); err != nil {
		log.Errorf("failed to stop HTTP server gracefully: %v", err)
	}

	// Stop background processes
	for _, closer := range closers {
		_ = closer.Close()
	}
}

func initLogger(cfg config.Log, writer io.Writer) {
	lvl, err := logrus.ParseLevel(cfg.Level)
	if err != nil {
		log.Panicf("invalid log level: %v", cfg.Level)
	}

	logrus.SetLevel(lvl)
	logrus.SetFormatter(&logrus.TextFormatter{})
	if cfg.Format == "json" {
		logrus.SetFormatter(&logrus.JSONFormatter{})
	}
	logrus.SetOutput(os.Stdout)
	if writer != nil {
		logrus.SetOutput(writer)
	}

	log = logrus.WithField("pkg", "main")
}
