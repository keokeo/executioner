# Executioner

> Backend service for Code Fighter.

Built using Go, MongoDB and Redis.

For setup development environment, please take a look at [CONTRIBUTING.md](https://gitlab.com/fighters-solution/fighters/blob/master/CONTRIBUTING.md) file.

### License

This project is under the MIT License. See the [LICENSE](https://gitlab.com/fighters-solution/executioner/blob/master/LICENSE) file for the full license text.
