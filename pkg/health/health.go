package health

import (
	"encoding/json"
	"net/http"
	"sync"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

const (
	OK        HealthStatus = "OK"
	UNHEALTHY HealthStatus = "UNHEALTHY"
)

var log = logrus.WithField("pkg", "health")

type (
	Checker interface {
		Ping() []*Endpoint
	}

	HealthStatus string

	Health struct {
		extChecks []Checker
		*sync.Mutex

		AppHealth
	}

	Endpoint struct {
		Name      string       `json:"name"`
		Addresses []string     `json:"addresses"`
		Status    HealthStatus `json:"status"`
		Error     string       `json:"error"`
	}

	Report struct {
		App              AppHealth   `json:"app"`
		ExternalServices []*Endpoint `json:"external_services"`
	}

	AppHealth struct {
		IsReadiness bool `json:"is_readiness"`
		IsLiveness  bool `json:"is_liveness"`
	}
)

func New(checks ...Checker) *Health {
	h := &Health{
		extChecks: checks,
		Mutex:     &sync.Mutex{},
	}
	return h
}

func (h *Health) RegisterHealthEndpoints(checks ...Checker) {
	if len(checks) == 0 {
		return
	}
	h.Lock()
	h.extChecks = append(h.extChecks, checks...)
	h.Unlock()
	log.Infof("%d new endpoint(s) registered, total %d endpoint(s)", len(checks), len(h.extChecks))
}

func (h *Health) Liveness(w http.ResponseWriter, r *http.Request) {
	if !h.IsLiveness {
		http.Error(w, string(UNHEALTHY), http.StatusInternalServerError)
		return
	}
	_, _ = w.Write([]byte(string(OK)))
}

func (h *Health) Readiness(w http.ResponseWriter, r *http.Request) {
	if !h.IsReadiness {
		http.Error(w, string(UNHEALTHY), http.StatusInternalServerError)
		return
	}
	_, _ = w.Write([]byte(string(OK)))
}

func (h *Health) SetStatus(isLiveness, isReadiness bool) {
	h.IsLiveness = isLiveness
	h.IsReadiness = isReadiness
}

func (h *Health) RegisterRoutes(r *http.ServeMux) {
	r.HandleFunc("/health/readiness", h.Readiness)
	r.HandleFunc("/health/liveness", h.Liveness)
	r.HandleFunc("/health/all", h.CheckAll)
}

// Health check for other external services
func (h *Health) CheckAll(w http.ResponseWriter, r *http.Request) {
	report := &Report{
		App: AppHealth{
			IsReadiness: h.IsReadiness,
			IsLiveness:  h.IsLiveness,
		},
		ExternalServices: []*Endpoint{},
	}

	for _, check := range h.extChecks {
		report.ExternalServices = append(report.ExternalServices, check.Ping()...)
	}

	b, err := json.Marshal(report)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, _ = w.Write(b)
}

// Default
var defaultHealth = New()

func RegisterHealthEndpoints(checks ...Checker) {
	defaultHealth.RegisterHealthEndpoints(checks...)
}
func SetStatus(isLiveness, isReadiness bool) {
	defaultHealth.SetStatus(isLiveness, isReadiness)
}
func Liveness(w http.ResponseWriter, r *http.Request) {
	defaultHealth.Liveness(w, r)
}
func Readiness(w http.ResponseWriter, r *http.Request) {
	defaultHealth.Readiness(w, r)
}
func CheckAll(w http.ResponseWriter, r *http.Request) {
	defaultHealth.CheckAll(w, r)
}
func RegisterRoutes(r chi.Router) {
	r.Get("/health/readiness", Readiness)
	r.Get("/health/liveness", Liveness)
	r.Get("/health/all", CheckAll)
	log.Infof("health APIs registered: /health/[readiness,liveness,all]")
}
