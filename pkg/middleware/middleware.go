package middleware

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("pkg", "middleware")

type Middleware func(http.Handler) http.Handler
