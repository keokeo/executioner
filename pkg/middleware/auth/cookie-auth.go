package auth

import (
	"encoding/json"
	"net/http"
	"sort"

	"github.com/go-chi/chi"
	"github.com/gorilla/sessions"
	"github.com/pkg/errors"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/pkg/middleware"
)

type ByCookie struct {
	store sessions.Store
}

func NewAuthByCookie(sessStore sessions.Store) *ByCookie {
	return &ByCookie{
		store: sessStore,
	}
}

func (c *ByCookie) Auth(roles ...model.UserRole) middleware.Middleware {
	if len(roles) == 0 {
		log.Panicf("authentication role(s) for API must be specified")
	}
	// Sort to ensure auth roles in strongest to weakest ordering
	sort.Slice(roles, func(i, j int) bool {
		return roles[i] < roles[j]
	})

	return func(next http.Handler) http.Handler {
		fn := func(w http.ResponseWriter, r *http.Request) {
			if err := c.authCheck(r, roles); err != nil {
				log.Errorf("auth failed: %s", err)
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		}
		return http.HandlerFunc(fn)
	}
}

func (c *ByCookie) authCheck(r *http.Request, roles []model.UserRole) error {
	sess, err := c.store.Get(r, model.CookieSession)
	if err != nil {
		return errors.Wrapf(err, "failed to get session from request")
	}
	u, ok := sess.Values["user"]
	if !ok {
		return errors.New("user not logged in yet")
	}

	us := model.UserSession{}
	if err := json.Unmarshal([]byte(u.(string)), &us); err != nil {
		return errors.Wrapf(err, "failed to decode user session")
	}
	uid := chi.URLParam(r, "uid")

	// API has permission for the owner of resource
	if roles[0] == model.Owner && us.UID == uid { // TODO
		return nil
	}

	for _, role := range roles {
		// API has permission for anyone who has role that is higher than or equal to
		// the API's role
		if model.UserRole(us.Role) <= role {
			return nil
		}
	}
	return errors.New("unauthorized")
}
