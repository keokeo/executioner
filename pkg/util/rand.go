package util

import (
	"math/rand"
	"sync"
)

// ConcurrentRand is the thread-safe wrapper for standard rand.Rand.
// The idea is basically the same as rand.lockedSource by guarding the underlying source
// using mutex.
type ConcurrentRand struct {
	r   *rand.Rand
	mux *sync.Mutex
}

func NewConcurrentRand(seed int64) *ConcurrentRand {
	return &ConcurrentRand{
		r:   rand.New(rand.NewSource(seed)),
		mux: &sync.Mutex{},
	}
}

// Seed uses the provided seed value to initialize the generator to a deterministic state.
// Seed should not be called concurrently with any other Rand method.
func (cr *ConcurrentRand) Seed(seed int64) {
	cr.mux.Lock()
	cr.r.Seed(seed)
	cr.mux.Unlock()
}

// Int63 returns a non-negative pseudo-random 63-bit integer as an int64.
func (cr *ConcurrentRand) Int63() int64 {
	cr.mux.Lock()
	i := cr.r.Int63()
	cr.mux.Unlock()
	return i
}

// Uint32 returns a pseudo-random 32-bit value as a uint32.
func (cr *ConcurrentRand) Uint32() uint32 {
	cr.mux.Lock()
	i := cr.r.Uint32()
	cr.mux.Unlock()
	return i
}

// Uint64 returns a pseudo-random 64-bit value as a uint64.
func (cr *ConcurrentRand) Uint64() uint64 {
	cr.mux.Lock()
	i := cr.r.Uint64()
	cr.mux.Unlock()
	return i
}

// Int31 returns a non-negative pseudo-random 31-bit integer as an int32.
func (cr *ConcurrentRand) Int31() int32 {
	cr.mux.Lock()
	i := cr.r.Int31()
	cr.mux.Unlock()
	return i
}

// Int returns a non-negative pseudo-random int.
func (cr *ConcurrentRand) Int() int {
	cr.mux.Lock()
	i := cr.r.Int()
	cr.mux.Unlock()
	return i
}

// Int63n returns, as an int64, a non-negative pseudo-random number in [0,n).
// It panics if n <= 0.
func (cr *ConcurrentRand) Int63n(n int64) int64 {
	cr.mux.Lock()
	i := cr.r.Int63n(n)
	cr.mux.Unlock()
	return i
}

// Int31n returns, as an int32, a non-negative pseudo-random number in [0,n).
// It panics if n <= 0.
func (cr *ConcurrentRand) Int31n(n int32) int32 {
	cr.mux.Lock()
	i := cr.r.Int31n(n)
	cr.mux.Unlock()
	return i
}

// Intn returns, as an int, a non-negative pseudo-random number in [0,n).
// It panics if n <= 0.
func (cr *ConcurrentRand) Intn(n int) int {
	cr.mux.Lock()
	i := cr.r.Intn(n)
	cr.mux.Unlock()
	return i
}

// Float64 returns, as a float64, a pseudo-random number in [0.0,1.0).
func (cr *ConcurrentRand) Float64() float64 {
	cr.mux.Lock()
	i := cr.r.Float64()
	cr.mux.Unlock()
	return i
}

// Float32 returns, as a float32, a pseudo-random number in [0.0,1.0).
func (cr *ConcurrentRand) Float32() float32 {
	cr.mux.Lock()
	i := cr.r.Float32()
	cr.mux.Unlock()
	return i
}

// Perm returns, as a slice of n ints, a pseudo-random permutation of the integers [0,n).
func (cr *ConcurrentRand) Perm(n int) []int {
	cr.mux.Lock()
	i := cr.r.Perm(n)
	cr.mux.Unlock()
	return i
}

// Shuffle pseudo-randomizes the order of elements.
// n is the number of elements. Shuffle panics if n < 0.
// swap swaps the elements with indexes i and j.
func (cr *ConcurrentRand) Shuffle(n int, swap func(i, j int)) {
	cr.mux.Lock()
	cr.r.Shuffle(n, swap)
	cr.mux.Unlock()
}

// Read generates len(p) random bytes and writes them into p. It
// always returns len(p) and a nil error.
// Read should not be called concurrently with any other Rand method.
func (cr *ConcurrentRand) Read(p []byte) (n int, err error) {
	cr.mux.Lock()
	n, err = cr.r.Read(p)
	cr.mux.Unlock()
	return
}
