package request

import (
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("pkg", "utils/request")

func Query(r *http.Request, key string) string {
	return r.URL.Query().Get(key)
}

// QueryInt returns the integer value of request's query key.
// Returns 0 if query not found or failed to convert query value to integer.
func QueryInt(r *http.Request, key string) int {
	s := r.URL.Query().Get(key)
	if s == "" {
		return 0
	}
	i, err := strconv.Atoi(s)
	if err != nil {
		log.Errorf("failed to convert to int: %v", err)
		return 0
	}
	return i
}
