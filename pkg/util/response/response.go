package response

import (
	"encoding/json"
	"net/http"

	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("pkg", "utils/response")

// JSON write status and JSON data to http response writer
func JSON(w http.ResponseWriter, status int, data interface{}) {
	b, err := json.Marshal(data)
	if err != nil {
		log.Errorf("failed to serialize JSON before response: %v", err)
		ErrorCode(w, http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	_, _ = w.Write(b)
}

// Error write error, status to http response writer
func Error(w http.ResponseWriter, err error, status int) {
	http.Error(w, err.Error(), status)
}

// ErrorCode responses error with HTTP code.
func ErrorCode(w http.ResponseWriter, code int) {
	http.Error(w, http.StatusText(code), code)
}
